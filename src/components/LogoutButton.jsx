import React from 'react';
import { useHistory } from 'react-router';
import { useApolloClient } from 'react-apollo';

export default function LogoutButton(props) {
	const history = useHistory();
	const client = useApolloClient();
	return (
		<button
			type='button'
			id='logout-button'
			onClick={() => {
				props.click();
				localStorage.clear();
				history.push(`/login`);
				client.resetStore();
			}}
		>
			Cerrar Sesión
		</button>
	);
}
