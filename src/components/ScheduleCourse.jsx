import React, { Component } from 'react';
import Moment from 'moment';

export class ScheduleCourse extends Component {
	state = {
		style: {
			gridArea: this.props.course.day,
			gridRow: `${Moment(
				this.props.course.startHour,
				'hh:mm:ss:SS'
			).hours() - 6} / ${Moment(
				this.props.course.finishHour,
				'hh:mm:ss:SS'
			).hours() - 5}`
		}
	};

	render() {
		const course = this.props.course;
		return (
			<div className='course' style={this.state.style}>
				<span className='event-date'>
					{Moment(course.startHour, 'hh:mm:ss:SS').format('HH:mm a')}{' '}
					-{' '}
					{Moment(course.finishHour, 'hh:mm:ss:SS').format('HH:mm a')}
				</span>
				<div>Curso: {course.group.course.name}</div>
				<div>Grupo: {course.group.denomination}</div>
				<div>
					Aula: {course.classroom.name || course.classroom.number} -{' '}
					{course.classroom.building.name}
				</div>
			</div>
		);
	}
}

export default ScheduleCourse;
