import React, { Fragment, useState, useCallback } from 'react';
import Navbar from '../components/Navbar';
import SideBar from '../components/SideBar';
import SideBarBackground from '../components/SideBarBackground';

export default function Menu() {
	const [open, setOpen] = useState(false);
	const toggleSideBar = useCallback(() => setOpen((value) => !value));

	return (
		<Fragment>
			<Navbar open={open} setOpen={toggleSideBar} />
			<SideBar open={open} setOpen={toggleSideBar} />
			<SideBarBackground open={open} setOpen={toggleSideBar} />
		</Fragment>
	);
}
