import React from 'react';
import { useQuery } from 'react-apollo';
import { USER_LOGGED_IN } from '../pages/App';
import { Redirect, Route } from 'react-router-dom';

export default function ProtectedRoute({ children, ...rest }) {
	const { loading, error } = useQuery(USER_LOGGED_IN);
	return (
		<Route
			{...rest}
			render={() => {
				if (loading) return 'Loading...';
				if (error) {
					if (error.message.includes('Not authenticated'))
						return <Redirect to='/login' />;
					return 'Error!';
				}
				return children;
			}}
		/>
	);
}
