import React, { useState } from 'react';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { useHistory } from 'react-router';

const LOGIN_MUTATION = gql`
	mutation LoginMutation($nickname: String!, $password: String!) {
		login(nickname: $nickname, password: $password) {
			token
			user {
				id
				name
				lastname
				email
				nickname
				photo
				state
			}
		}
	}
`;

function loginHandle(user, history) {
	if (user) {
		localStorage.setItem('AUTH_TOKEN', JSON.stringify(user.token));
		history.push('/');
	}
}

export default function LoginForm() {
	const [nickname, setNickname] = useState('');
	const [password, setPassword] = useState('');
	const history = useHistory();

	return (
		<form>
			<div>
				<label htmlFor='nickname'>Nickname:</label>
				<input
					value={nickname}
					onChange={e => setNickname(e.target.value)}
					type='text'
					name='nickname'
					id='nickname'
				/>
			</div>
			<div>
				<label htmlFor='password'>Password:</label>
				<input
					value={password}
					onChange={e => setPassword(e.target.value)}
					type='password'
					name='password'
					id='password'
				/>
			</div>
			<div>
				<Mutation
					mutation={LOGIN_MUTATION}
					variables={{ nickname, password }}
					onCompleted={({ login }) => loginHandle(login, history)}
				>
					{mutation => (
						<button onClick={mutation} type='button'>
							Login
						</button>
					)}
				</Mutation>
				<button type='button'>Sign up</button>
			</div>
		</form>
	);
}
