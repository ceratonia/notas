import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import LogoutButton from './LogoutButton';
import { useQuery } from 'react-apollo';
import { USER_LOGGED_IN } from '../pages/App';

export default function SideBar({ open, setOpen }) {
	const { loading, error, data } = useQuery(USER_LOGGED_IN);
	if (loading) return 'Loading';
	if (error) return 'Error';

	return (
		<div className={open ? 'sidebar open' : 'sidebar'}>
			<Link to='/profile' className='user' onClick={setOpen}>
				<img className='user-profile' src={data.me.photo} alt='user' />
				<span>{data.me.nickname}</span>
			</Link>
			<ul>
				<li>
					<NavLink
						exact
						to='/enrollment'
						activeClassName='sidebar-item-active'
						onClick={setOpen}
					>
						Matrícula
					</NavLink>
				</li>
				<li>
					<NavLink
						exact
						to='/teacher'
						activeClassName='sidebar-item-active'
						onClick={setOpen}
					>
						Profesores
					</NavLink>
				</li>
				<li>
					<NavLink
						exact
						to='/schedule'
						activeClassName='sidebar-item-active'
						onClick={setOpen}
					>
						Horario
					</NavLink>
				</li>
				<li>
					<NavLink
						exact
						to='/configuration'
						activeClassName='sidebar-item-active'
						onClick={setOpen}
					>
						Configuración
					</NavLink>
				</li>
			</ul>
			<LogoutButton click={setOpen} />
		</div>
	);
}
