import React from 'react';

export default function ToggleSideBarButton({ open, setOpen }) {
	return (
		<button id='toggle-button' type='button' onClick={setOpen}>
			<span></span>
			<span></span>
			<span></span>
		</button>
	);
}
