import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { gql } from 'apollo-boost';
import { useMutation } from 'react-apollo';
import { ACTUAL_SEMESTER } from '../pages/App';
import { ENROLLMENT_QUERY } from '../pages/Enrollment';

const DELETE_ENROLLMENT_DETAIL = gql`
	mutation DeleteEnrollmentDetail($id: ID!) {
		deleteEnrollmentDetail(id: $id) {
			id
		}
	}
`;

function DeleteButton({ id, click }) {
	const [mutate] = useMutation(DELETE_ENROLLMENT_DETAIL, {
		variables: { id },
		update: (store) => {
			const { actualSemester } = store.readQuery({
				query: ACTUAL_SEMESTER,
			});
			const { enrollment } = store.readQuery({
				query: ENROLLMENT_QUERY,
				variables: { semester: actualSemester.id },
			});
			enrollment.groups.splice(
				enrollment.groups.findIndex((i) => i.id === id),
				1
			);
			store.writeQuery({
				query: ENROLLMENT_QUERY,
				variables: { semester: actualSemester.id },
				data: { enrollment },
			});
			click(enrollment.groups);
		},
	});
	return (
		<button type='button' onClick={mutate}>
			Eliminar
		</button>
	);
}

export default function CourseElement({ group, id, onDeleteGroup }) {
	return (
		<div className='course-element'>
			<DeleteButton id={id} click={onDeleteGroup} />
			<Link to={`/course/${group.course.id}`}>Ver más</Link>
			<h1>{group.course.name}</h1>
			<span>Ciclo {group.course.academicPhase}</span>
			<span>N° Créditos: {group.course.credits}</span>
			<h1>
				Profesor: {group.teacher.name} {group.teacher.lastname}
			</h1>
		</div>
	);
}
