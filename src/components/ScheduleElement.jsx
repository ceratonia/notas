import React from 'react';
import Moment from 'moment';
import ScheduleCourse from './ScheduleCourse';

function createTimeLine() {
	const timeLine = [];
	const moment = Moment(0);
	for (let i = 7; i < 24; i++) {
		timeLine.push(<li key={i}>{moment.hours(i).format('HH:mm a')}</li>);
	}
	return timeLine;
}

export default function ScheduleElement({ state }) {
	const { loading, error, data } = state;
	if (loading) return 'Loading';
	if (error) return 'Error';
	return (
		<div id='schedule'>
			<div className='week-days'>
				<span>LUNES</span>
				<span>MARTES</span>
				<span>MIÉRCOLES</span>
				<span>JUEVES</span>
				<span>VIERNES</span>
				<span>SÁBADOS</span>
			</div>
			<ul className='timeline'>{createTimeLine().map(_ => _)}</ul>
			<div className='content'>
				{data.schedules.map(course => (
					<ScheduleCourse key={course.id} course={course} />
				))}
				<div
					style={{
						gridArea: 'LUNES',
						gridRow: '1 / -1',
						border: '1px solid gray'
					}}
				></div>
				<div
					style={{
						gridArea: 'MARTES',
						gridRow: '1 / -1',
						border: '1px solid gray'
					}}
				></div>
				<div
					style={{
						gridArea: 'MIERCOLES',
						gridRow: '1 / -1',
						border: '1px solid gray'
					}}
				></div>
				<div
					style={{
						gridArea: 'JUEVES',
						gridRow: '1 / -1',
						border: '1px solid gray'
					}}
				></div>
				<div
					style={{
						gridArea: 'VIERNES',
						gridRow: '1 / -1',
						border: '1px solid gray'
					}}
				></div>
				<div
					style={{
						gridArea: 'SABADO',
						gridRow: '1 / -1',
						border: '1px solid gray'
					}}
				></div>
			</div>
		</div>
	);
}
