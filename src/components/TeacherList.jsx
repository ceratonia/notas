import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import TeacherElement from './TeacherElement';

const TEACHERS_QUERY = gql`
	{
		teachers {
			id
			name
			lastname
			rating
		}
	}
`;

export class TeacherList extends Component {
	render() {
		return (
			<Query query={TEACHERS_QUERY}>
				{({ loading, error, data }) => {
					if (loading) return <h1>Loading...</h1>;
					if (error) return <h1>Error!</h1>;
					const teachers = data.teachers;
					return (
						<div className='grid'>
							{teachers.map(teacher => (
								<TeacherElement
									key={teacher.id}
									teacher={teacher}
								/>
							))}
						</div>
					);
				}}
			</Query>
		);
	}
}

export default TeacherList;
