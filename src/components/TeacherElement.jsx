import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class TeacherElement extends Component {
	render() {
		const teacher = this.props.teacher;
		return (
			<Link to={`/teacher/${teacher.id}`} className='teacher-element'>
				<h1>
					{teacher.name}, {teacher.lastname}
				</h1>
				<span>Rating: {teacher.rating || 'Sin rating'}</span>
			</Link>
		);
	}
}

export default TeacherElement;
