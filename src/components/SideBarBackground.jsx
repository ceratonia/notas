import React from 'react';

export default function SideBarBackground({ open, setOpen }) {
	return (
		<div
			id='sidebar-background'
			className={open ? 'open' : ''}
			onClick={setOpen}
		/>
	);
}
