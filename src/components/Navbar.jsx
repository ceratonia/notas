import React from 'react';
import { Link } from 'react-router-dom';
import ToggleSideBarButton from './ToggleSideBarButton';
import { useQuery } from 'react-apollo';
import { USER_LOGGED_IN } from '../pages/App';

export default function Navbar({ open, setOpen }) {
	const { loading, error, data } = useQuery(USER_LOGGED_IN);
	if (loading) return 'Loading';
	if (error) return 'Error';
	return (
		<nav>
			<ToggleSideBarButton open={open} setOpen={setOpen} />
			<Link to='/' className='logo'>
				Anota
			</Link>
			<Link to='/profile' className='user'>
				<img className='user-profile' src={data.me.photo} alt='user' />
				<span>{data.me.nickname}</span>
			</Link>
		</nav>
	);
}
