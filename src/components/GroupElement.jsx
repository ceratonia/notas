import React from 'react';

export default function GroupElement({ group, click, selected }) {
	return (
		<div onClick={() => click(group.id)}>
			{selected && <span>Seleccionado</span>}
			<h4>
				{group.course.name} ({group.denomination})
			</h4>
			<span>Ciclo: {group.course.academicPhase}</span>
			<h5>
				Profesor(a): {group.teacher.lastname}, {group.teacher.name}
			</h5>
			<span>Rating: {group.teacher.rating || 'Sin rating'}</span>
		</div>
	);
}
