import React, { useState } from 'react';
import CourseElement from './CourseElement';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { ACTUAL_SEMESTER } from '../pages/App';
import { ENROLLMENT_QUERY } from '../pages/Enrollment';
import { Link } from 'react-router-dom';

const ADD_ENROLLMENT = gql`
	mutation AddEnrollment($semester: ID!) {
		createEnrollment(semester: $semester) {
			id
			groups {
				id
				group {
					id
					course {
						name
					}
				}
			}
		}
	}
`;

const DELETE_ENROLLMENT_DETAIL = gql`
	mutation DeleteEnrollmentDetail($id: ID!) {
		deleteEnrollmentDetail(id: $id) {
			id
		}
	}
`;

function AddEnrollmentButton() {
	const {
		data: { actualSemester },
	} = useQuery(ACTUAL_SEMESTER);
	const [mutate] = useMutation(ADD_ENROLLMENT, {
		variables: { semester: actualSemester.id },
		update: (store, { data: { createEnrollment } }) => {
			store.writeQuery({
				query: ENROLLMENT_QUERY,
				variables: { semester: actualSemester.id },
				data: { enrollment: createEnrollment },
			});
		},
	});
	return (
		<button type='button' onClick={mutate}>
			Agregamos una?
		</button>
	);
}

export default function CourseList({ enrollment }) {
	const [courseList, setCourseList] = useState(enrollment.groups);

	function deleteGroupHandle(groups) {
		setCourseList(groups);
	}

	if (enrollment) {
		return (
			<div>
				<div>
					<Link to='/enrollment/add'>Agregar cursos</Link>
				</div>
				<div className='grid'>
					{courseList.map((group) => (
						<CourseElement
							key={group.id}
							group={group.group}
							id={group.id}
							onDeleteGroup={deleteGroupHandle}
						/>
					))}
				</div>
			</div>
		);
	} else {
		return (
			<div>
				<h1>Parecer ser que no tienes una matrícula :(</h1>
				<AddEnrollmentButton />
			</div>
		);
	}
}
