import React, { useState } from 'react';
import { gql } from 'apollo-boost';
import { useQuery, useMutation } from 'react-apollo';
import GroupElement from '../components/GroupElement';
import { ACTUAL_SEMESTER } from './App';
import { ENROLLMENT_QUERY } from './Enrollment';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

const GROUPS_QUERY = gql`
	query GroupsQuery($semester: ID!) {
		groups(semester: $semester) {
			id
			denomination
			course {
				id
				name
				academicPhase
			}
			teacher {
				id
				name
				lastname
				rating
			}
		}
	}
`;

const CREATE_ENROLLMENT_DETAIL = gql`
	mutation CreateEnrollmentDetail($enrollment: ID!, $groups: [ID!]!) {
		createEnrollmentDetail(enrollment: $enrollment, groups: $groups) {
			id
			group {
				id
				denomination
				course {
					id
					name
					credits
					academicPhase
				}
				teacher {
					id
					name
					lastname
					rating
				}
			}
		}
	}
`;

export default function EnrollmentAdd({ enrollmentId }) {
	const [coursesSelected, setCoursesSelected] = useState([]);
	function groupClickHandle(id) {
		setCoursesSelected((value) =>
			value.includes(id) ? value : value.concat([id])
		);
	}
	const history = useHistory();
	const {
		data: { actualSemester },
	} = useQuery(ACTUAL_SEMESTER);
	const {
		data: { enrollment },
	} = useQuery(ENROLLMENT_QUERY, {
		variables: { semester: actualSemester.id },
	});
	const { loading, error, data } = useQuery(GROUPS_QUERY, {
		variables: { semester: actualSemester.id },
	});
	const [mutate] = useMutation(CREATE_ENROLLMENT_DETAIL, {
		variables: { enrollment: enrollmentId, groups: coursesSelected },
		update: (store, { data: { createEnrollmentDetail } }) => {
			const data = store.readQuery({
				query: ENROLLMENT_QUERY,
				variables: { semester: actualSemester.id },
			});
			data.enrollment.groups.push(...createEnrollmentDetail);
			store.writeQuery({
				query: ENROLLMENT_QUERY,
				variables: { semester: actualSemester.id },
				data,
			});
			history.push('/enrollment');
		},
	});

	if (loading) return <h1>Loading</h1>;
	if (error) return <h1>Error</h1>;
	return (
		<div>
			<h1>Selecciona los cursos que te vas a matricular</h1>
			<Link to='/group/add'>Registrar grupo</Link>
			<button type='button' onClick={mutate}>
				Guardar
			</button>
			{data.groups.map((group) => (
				<GroupElement
					key={group.id}
					group={group}
					selected={enrollment.groups.some(
						(g) => g.group.id === group.id
					)}
					click={groupClickHandle}
				/>
			))}
		</div>
	);
}
