import React, { Component, Fragment } from 'react';
import TeacherList from '../components/TeacherList';

export class Teacher extends Component {
	render() {
		return (
			<Fragment>
				<h1>Lista de profesores</h1>
				<TeacherList />
			</Fragment>
		);
	}
}

export default Teacher;
