import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './Login';
import Main from './Main';
import Profile from './Profile';
import Enrollment from './Enrollment';
import Teacher from './Teacher';
import Schedule from './Schedule';
import Configuration from './Configuration';
import { gql } from 'apollo-boost';
import ProtectedRoute from '../components/ProtectedRoute';
import { useQuery } from 'react-apollo';
import Menu from '../components/Menu';

export const USER_LOGGED_IN = gql`
	query UserLoggedIn {
		me {
			id
			name
			lastname
			nickname
			photo
		}
	}
`;

export const ACTUAL_SEMESTER = gql`
	query ActualSemester {
		actualSemester {
			id
			name
		}
	}
`;

export default function App() {
	const { loading, error } = useQuery(ACTUAL_SEMESTER);
	if (loading) return 'Loading';
	if (error) return 'Error';
	return (
		<Switch>
			<Route exact path='/login'>
				<Login />
			</Route>
			<Fragment>
				<Menu />
				<Switch>
					<ProtectedRoute exact path='/'>
						<Main />
					</ProtectedRoute>
					<ProtectedRoute exact path='/profile'>
						<Profile />
					</ProtectedRoute>
					<ProtectedRoute path='/enrollment'>
						<Enrollment />
					</ProtectedRoute>
					<ProtectedRoute exact path='/teacher'>
						<Teacher />
					</ProtectedRoute>
					<ProtectedRoute exact path='/schedule'>
						<Schedule />
					</ProtectedRoute>
					<ProtectedRoute exact path='/configuration'>
						<Configuration />
					</ProtectedRoute>
				</Switch>
			</Fragment>
		</Switch>
	);
}
