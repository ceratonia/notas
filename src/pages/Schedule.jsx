import React, { Fragment } from 'react';
import ScheduleElement from '../components/ScheduleElement';
import { gql } from 'apollo-boost';
import { useQuery } from 'react-apollo';
import { ACTUAL_SEMESTER } from './App';

export const ALL_SEMESTERS = gql`
	query AllSemesters {
		semesters {
			id
			name
		}
	}
`;

export const SCHEDULE_QUERY = gql`
	query Schedule($semester: ID!) {
		schedules(semester: $semester) {
			id
			day
			startHour
			finishHour
			group {
				id
				denomination
				course {
					id
					name
				}
			}
			classroom {
				id
				name
				number
				building {
					name
				}
			}
		}
	}
`;

function SelectSemester({ state, actualSemester, fetchMore }) {
	const { loading, error, data } = state;
	if (loading) return 'Loading';
	if (error) return 'Error';
	return (
		<select
			defaultValue={actualSemester.id}
			onChange={({ target }) =>
				fetchMore({
					variables: { semester: target.value },
					updateQuery: (prev, { fetchMoreResult }) => {
						if (!fetchMoreResult) return prev;
						return fetchMoreResult;
					}
				})
			}
		>
			{data.semesters.map(semester => (
				<option key={semester.id} value={semester.id}>
					{semester.name}
				</option>
			))}
		</select>
	);
}

export default function Schedule() {
	const {
		data: { actualSemester }
	} = useQuery(ACTUAL_SEMESTER);
	const allSemestersState = useQuery(ALL_SEMESTERS);
	const scheduleState = useQuery(SCHEDULE_QUERY, {
		variables: {
			semester: actualSemester.id
		}
	});
	return (
		<Fragment>
			<h1>Horario del usuario</h1>
			<SelectSemester
				fetchMore={scheduleState.fetchMore}
				actualSemester={actualSemester}
				state={allSemestersState}
			/>
			<ScheduleElement state={scheduleState} />
		</Fragment>
	);
}
