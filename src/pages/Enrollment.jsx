import React, { Fragment } from 'react';
import CourseList from '../components/CourseList';
import { Route, withRouter, Switch } from 'react-router';
import EnrollmentAdd from './EnrollmentAdd';
import { gql } from 'apollo-boost';
import { useQuery } from 'react-apollo';
import { ACTUAL_SEMESTER } from './App';

export const ENROLLMENT_QUERY = gql`
	query EnrollementQuery($semester: ID!) {
		enrollment(semester: $semester) {
			id
			groups {
				id
				group {
					id
					course {
						id
						name
						academicPhase
						credits
					}
					teacher {
						id
						name
						lastname
					}
				}
			}
		}
	}
`;

function Enrollment({ match }) {
	const {
		data: { actualSemester }
	} = useQuery(ACTUAL_SEMESTER);
	const { loading, error, data } = useQuery(ENROLLMENT_QUERY, {
		variables: { semester: actualSemester.id }
	});
	if (loading) return <h1>Loading...</h1>;
	if (error) return <h1>Error!</h1>;
	return (
		<Fragment>
			<h1>Lista de cursos</h1>
			<Switch>
				<Route exact path={`${match.path}/`}>
					<CourseList enrollment={data.enrollment} />
				</Route>
				<Route exact path={`${match.path}/add`}>
					<EnrollmentAdd enrollmentId={data.enrollment.id} />
				</Route>
			</Switch>
		</Fragment>
	);
}

export default withRouter(Enrollment);
