# Migration `20200220124831-init`

This migration has been generated at 2/20/2020, 12:48:31 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE "public"."Faculty" (
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."School" (
    "faculty" integer  NOT NULL ,
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Semester" (
    "finishDate" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    "startDate" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."User" (
    "birthdate" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    "createdAt" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    "email" text  NOT NULL DEFAULT '',
    "gender" text  NOT NULL DEFAULT '',
    "id" SERIAL,
    "lastname" text  NOT NULL DEFAULT '',
    "name" text  NOT NULL DEFAULT '',
    "nickname" text  NOT NULL DEFAULT '',
    "password" text  NOT NULL DEFAULT '',
    "photo" text   ,
    "school" integer  NOT NULL ,
    "semesterIn" integer  NOT NULL ,
    "updatedAt" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Course" (
    "academicPhase" integer  NOT NULL DEFAULT 0,
    "code" text  NOT NULL DEFAULT '',
    "createdAt" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    "credits" integer  NOT NULL DEFAULT 0,
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    "prerequisiteOf" integer  NOT NULL ,
    "school" integer  NOT NULL ,
    "updatedAt" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Teacher" (
    "email" text  NOT NULL DEFAULT '',
    "id" SERIAL,
    "lastname" text  NOT NULL DEFAULT '',
    "name" text  NOT NULL DEFAULT '',
    "rating" Decimal(65,30)   ,
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Group" (
    "course" integer  NOT NULL ,
    "denomination" text  NOT NULL DEFAULT '',
    "id" SERIAL,
    "rating" Decimal(65,30)   ,
    "semester" integer  NOT NULL ,
    "teacher" integer  NOT NULL ,
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Enrollment" (
    "averageGrade" integer   ,
    "group" integer  NOT NULL ,
    "id" SERIAL,
    "rating" integer   ,
    "user" integer  NOT NULL ,
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."TipeActivity" (
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Activity" (
    "finishDate" timestamp(3)   ,
    "group" integer  NOT NULL ,
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    "obligatory" boolean  NOT NULL DEFAULT false,
    "parentActivity" integer  NOT NULL ,
    "startDate" timestamp(3)  NOT NULL DEFAULT '1970-01-01 00:00:00',
    "typeActivity" integer  NOT NULL ,
    "weight" Decimal(65,30)  NOT NULL DEFAULT 0,
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Grade" (
    "activity" integer  NOT NULL ,
    "calification" Decimal(65,30)   ,
    "confirmated" boolean  NOT NULL DEFAULT false,
    "enrollment" integer  NOT NULL ,
    "id" SERIAL,
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Building" (
    "id" SERIAL,
    "name" text  NOT NULL DEFAULT '',
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Classroom" (
    "building" integer  NOT NULL ,
    "id" SERIAL,
    "name" text   ,
    "number" text   ,
    PRIMARY KEY ("id")
) 

CREATE TABLE "public"."Schedule" (
    "classroom" integer  NOT NULL ,
    "day" text  NOT NULL DEFAULT '',
    "finishHour" text  NOT NULL DEFAULT '',
    "group" integer  NOT NULL ,
    "id" SERIAL,
    "startHour" text  NOT NULL DEFAULT '',
    PRIMARY KEY ("id")
) 

CREATE UNIQUE INDEX "Faculty.name" ON "public"."Faculty"("name")

CREATE UNIQUE INDEX "School.name" ON "public"."School"("name")

CREATE UNIQUE INDEX "Semester.name" ON "public"."Semester"("name")

CREATE UNIQUE INDEX "User.nickname" ON "public"."User"("nickname")

CREATE UNIQUE INDEX "User.email" ON "public"."User"("email")

CREATE UNIQUE INDEX "Course.code" ON "public"."Course"("code")

CREATE UNIQUE INDEX "Teacher.email" ON "public"."Teacher"("email")

CREATE UNIQUE INDEX "TipeActivity.name" ON "public"."TipeActivity"("name")

ALTER TABLE "public"."School" ADD FOREIGN KEY ("faculty")REFERENCES "public"."Faculty"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."User" ADD FOREIGN KEY ("school")REFERENCES "public"."School"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."User" ADD FOREIGN KEY ("semesterIn")REFERENCES "public"."Semester"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Course" ADD FOREIGN KEY ("school")REFERENCES "public"."School"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Course" ADD FOREIGN KEY ("prerequisiteOf")REFERENCES "public"."Course"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Group" ADD FOREIGN KEY ("semester")REFERENCES "public"."Semester"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Group" ADD FOREIGN KEY ("course")REFERENCES "public"."Course"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Group" ADD FOREIGN KEY ("teacher")REFERENCES "public"."Teacher"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Enrollment" ADD FOREIGN KEY ("user")REFERENCES "public"."User"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Enrollment" ADD FOREIGN KEY ("group")REFERENCES "public"."Group"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Activity" ADD FOREIGN KEY ("group")REFERENCES "public"."Group"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Activity" ADD FOREIGN KEY ("typeActivity")REFERENCES "public"."TipeActivity"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Activity" ADD FOREIGN KEY ("parentActivity")REFERENCES "public"."Activity"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Grade" ADD FOREIGN KEY ("enrollment")REFERENCES "public"."Enrollment"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Grade" ADD FOREIGN KEY ("activity")REFERENCES "public"."Activity"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Classroom" ADD FOREIGN KEY ("building")REFERENCES "public"."Building"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Schedule" ADD FOREIGN KEY ("group")REFERENCES "public"."Group"("id") ON DELETE RESTRICT  ON UPDATE CASCADE

ALTER TABLE "public"."Schedule" ADD FOREIGN KEY ("classroom")REFERENCES "public"."Classroom"("id") ON DELETE RESTRICT  ON UPDATE CASCADE
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration ..20200220124831-init
--- datamodel.dml
+++ datamodel.dml
@@ -1,0 +1,134 @@
+datasource db {
+    provider = "postgresql"
+    url      = "postgresql://postgres:123456789@localhost:5454/notas"
+}
+
+model Faculty {
+    id      Int      @id @default(autoincrement())
+    name    String   @unique
+    schools School[]
+}
+
+model School {
+    id      Int      @id @default(autoincrement())
+    name    String   @unique
+    faculty Faculty
+    courses Course[]
+}
+
+model Semester {
+    id         Int      @id @default(autoincrement())
+    name       String   @unique
+    startDate  DateTime
+    finishDate DateTime
+}
+
+model User {
+    id          Int          @id @default(autoincrement())
+    nickname    String       @unique
+    password    String
+    email       String       @unique
+    name        String
+    lastname    String
+    gender      String
+    birthdate   DateTime
+    photo       String?
+    semesterIn  Semester
+    school      School
+    enrollments Enrollment[]
+    createdAt   DateTime     @default(now())
+    updatedAt   DateTime     @updatedAt
+}
+
+model Course {
+    id             Int      @id @default(autoincrement())
+    name           String
+    code           String   @unique
+    academicPhase  Int
+    credits        Int
+    school         School
+    prerequisiteOf Course   @relation("course_has_prerequisites")
+    prerequisites  Course[] @relation("course_has_prerequisites")
+    createdAt      DateTime @default(now())
+    updatedAt      DateTime @updatedAt
+}
+
+model Teacher {
+    id       Int     @id @default(autoincrement())
+    name     String
+    lastname String
+    email    String  @unique
+    rating   Float?
+    groups   Group[]
+}
+
+model Group {
+    id           Int        @id @default(autoincrement())
+    denomination String
+    rating       Float?
+    semester     Semester
+    teacher      Teacher
+    course       Course
+    activities   Activity[]
+    schedules    Schedule[]
+}
+
+model Enrollment {
+    id           Int     @id @default(autoincrement())
+    averageGrade Int?
+    rating       Int?
+    group        Group
+    user         User
+    grades       Grade[]
+}
+
+model TipeActivity {
+    id   Int    @id @default(autoincrement())
+    name String @unique
+}
+
+model Activity {
+    id             Int          @id @default(autoincrement())
+    name           String
+    weight         Float
+    startDate      DateTime
+    finishDate     DateTime?
+    obligatory     Boolean
+    parentActivity Activity     @relation("activity_has_activities")
+    activities     Activity[]   @relation("activity_has_activities")
+    typeActivity   TipeActivity
+    group          Group
+}
+
+model Grade {
+    id           Int        @id @default(autoincrement())
+    calification Float?
+    confirmated  Boolean    @default(false)
+    activity     Activity
+    enrollment   Enrollment
+}
+
+model Building {
+    id   Int    @id @default(autoincrement())
+    name String
+}
+
+model Classroom {
+    id       Int      @id @default(autoincrement())
+    name     String?
+    number   String?
+    building Building
+}
+
+model Schedule {
+    id         Int       @id @default(autoincrement())
+    day        String
+    startHour  String
+    finishHour String
+    group      Group
+    classroom  Classroom
+}
+
+generator client {
+    provider = "prisma-client-js"
+}
```


