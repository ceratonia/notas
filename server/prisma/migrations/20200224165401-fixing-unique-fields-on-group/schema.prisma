datasource db {
    provider = "postgresql"
    url = "***"
}

model Faculty {
    id      Int      @id @default(autoincrement())
    name    String   @unique
    state   Boolean  @default(true)
    schools School[]
}

model School {
    id      Int      @id @default(autoincrement())
    name    String   @unique
    state   Boolean  @default(true)
    faculty Faculty
    courses Course[]
}

model Semester {
    id         Int       @id @default(autoincrement())
    name       String    @unique
    startDate  DateTime?
    finishDate DateTime?
    state      Boolean   @default(false)
}

model User {
    id               Int          @id @default(autoincrement())
    nickname         String       @unique
    password         String
    email            String       @unique
    name             String
    lastname         String
    gender           String
    birthDate        DateTime
    photo            String?
    semesterIn       Semester
    school           School
    state            Boolean      @default(true)
    enrollments      Enrollment[]
    createdAt        DateTime     @default(now())
    updatedAt        DateTime     @updatedAt
    coursesCreated   Course[]     @relation("courseCreatedByUser")
    coursesUpdated   Course[]     @relation("courseUpdatedByUser")
    teachersCreated  Teacher[]    @relation("teacherCreatedByUser")
    teachersUpdated  Teacher[]    @relation("teacherUpdatedByUser")
    groupsCreated    Group[]      @relation("groupCreatedByUser")
    groupsUpdated    Group[]      @relation("groupUpdatedByUser")
    activitysCreated Activity[]   @relation("activityCreatedByUser")
    activitysUpdated Activity[]   @relation("activityUpdatedByUser")
    schedulesCreated Schedule[]   @relation("scheduleCreatedByUser")
    schedulesUpdated Schedule[]   @relation("scheduleUpdatedByUser")
}

model Course {
    id             Int      @id @default(autoincrement())
    name           String
    code           String   @unique
    academicPhase  Int
    credits        Int
    school         School
    prerequisiteOf Course[] @relation("CoursePrerequisite")
    prerequisites  Course[] @relation("CoursePrerequisite")
    createdAt      DateTime @default(now())
    updatedAt      DateTime @updatedAt
    createdBy      User     @relation("courseCreatedByUser")
    updatedBy      User     @relation("courseUpdatedByUser")
}

model Teacher {
    id        Int      @id @default(autoincrement())
    name      String
    lastname  String
    email     String   @unique
    rating    Float?
    state     Boolean  @default(true)
    createdAt DateTime @default(now())
    updatedAt DateTime @updatedAt
    createdBy User     @relation("teacherCreatedByUser")
    updatedBy User     @relation("teacherUpdatedByUser")
    groups    Group[]
}

model Group {
    id           Int        @id @default(autoincrement())
    denomination String
    rating       Float?
    state        Boolean    @default(true)
    semester     Semester
    teacher      Teacher
    course       Course
    createdAt    DateTime   @default(now())
    updatedAt    DateTime   @updatedAt
    createdBy    User       @relation("groupCreatedByUser")
    updatedBy    User       @relation("groupUpdatedByUser")
    activities   Activity[]
    schedules    Schedule[]

    @@unique([semester, teacher, course, denomination])
}

model Enrollment {
    id           Int      @id @default(autoincrement())
    averageGrade Int?
    rating       Int?
    state        Boolean  @default(true)
    group        Group
    user         User
    createdAt    DateTime @default(now())
    updatedAt    DateTime @updatedAt
    grades       Grade[]

    @@index([user, group])
}

model TypeActivity {
    id   Int    @id @default(autoincrement())
    name String @unique
}

model Activity {
    id             Int          @id @default(autoincrement())
    name           String
    weight         Float
    startDate      DateTime
    finishDate     DateTime?
    obligatory     Boolean      @default(true)
    state          Boolean      @default(true)
    parentActivity Activity?    @relation("activity_has_activities")
    typeActivity   TypeActivity
    group          Group
    createdAt      DateTime     @default(now())
    updatedAt      DateTime     @updatedAt
    createdBy      User         @relation("activityCreatedByUser")
    updatedBy      User         @relation("activityUpdatedByUser")
    activities     Activity[]   @relation("activity_has_activities")
}

model Grade {
    id           Int        @id @default(autoincrement())
    calification Float?
    confirmated  Boolean    @default(false)
    state        Boolean    @default(true)
    activity     Activity
    enrollment   Enrollment
    createdAt    DateTime   @default(now())
    updatedAt    DateTime   @updatedAt
}

model Building {
    id    Int     @id @default(autoincrement())
    name  String  @unique
    state Boolean @default(true)
}

model Classroom {
    id       Int      @id @default(autoincrement())
    name     String?
    number   String?
    state    Boolean  @default(true)
    building Building
}

enum Day {
    LUNES  MARTES  MIERCOLES
    @map("MIÉRCOLES")
    JUEVES VIERNES SABADO @map("SÁBADO")
}

model Schedule {
    id         Int       @id @default(autoincrement())
    day        Day
    startHour  DateTime
    finishHour DateTime
    state      Boolean   @default(true)
    group      Group
    classroom  Classroom
    createdAt  DateTime  @default(now())
    updatedAt  DateTime  @updatedAt
    createdBy  User      @relation("scheduleCreatedByUser")
    updatedBy  User      @relation("scheduleUpdatedByUser")
}

generator client {
    provider = "prisma-client-js"
}