datasource db {
  provider = "postgres"
  url = "***"
}

model Faculty {
  id      Int      @default(autoincrement()) @id
  name    String   @unique
  state   Boolean  @default(true)
  schools School[]
}

model School {
  id      Int      @default(autoincrement()) @id
  name    String   @unique
  state   Boolean  @default(true)
  faculty Faculty  @relation(references: [id])
  courses Course[]
  users   User[]
}

model Semester {
  id          Int          @default(autoincrement()) @id
  name        String       @unique
  startDate   DateTime?
  finishDate  DateTime?
  state       Boolean      @default(false)
  users       User[]
  groups      Group[]
  enrollments Enrollment[]
}

model User {
  id               Int          @default(autoincrement()) @id
  nickname         String       @unique
  password         String
  email            String       @unique
  name             String
  lastname         String
  gender           String
  birthDate        DateTime
  photo            String?
  semesterIn       Semester     @relation(references: [id])
  school           School       @relation(references: [id])
  state            Boolean      @default(true)
  enrollments      Enrollment[]
  createdAt        DateTime     @default(now())
  updatedAt        DateTime     @updatedAt
  coursesCreated   Course[]     @relation("courseCreatedByUser")
  coursesUpdated   Course[]     @relation("courseUpdatedByUser")
  teachersCreated  Teacher[]    @relation("teacherCreatedByUser")
  teachersUpdated  Teacher[]    @relation("teacherUpdatedByUser")
  groupsCreated    Group[]      @relation("groupCreatedByUser")
  groupsUpdated    Group[]      @relation("groupUpdatedByUser")
  activitysCreated Activity[]   @relation("activityCreatedByUser")
  activitysUpdated Activity[]   @relation("activityUpdatedByUser")
  schedulesCreated Schedule[]   @relation("scheduleCreatedByUser")
  schedulesUpdated Schedule[]   @relation("scheduleUpdatedByUser")
}

model Course {
  id             Int      @default(autoincrement()) @id
  name           String
  code           String   @unique
  academicPhase  Int
  credits        Int
  school         School   @relation(references: [id])
  prerequisiteOf Course[] @relation("CoursePrerequisite", references: [id])
  prerequisites  Course[] @relation("CoursePrerequisite", references: [id])
  createdAt      DateTime @default(now())
  updatedAt      DateTime @updatedAt
  createdBy      User     @relation("courseCreatedByUser", references: [id])
  updatedBy      User     @relation("courseUpdatedByUser", references: [id])
  groups         Group[]
}

model Teacher {
  id        Int      @default(autoincrement()) @id
  name      String
  lastname  String
  email     String   @unique
  rating    Float?
  state     Boolean  @default(true)
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  createdBy User     @relation("teacherCreatedByUser", references: [id])
  updatedBy User     @relation("teacherUpdatedByUser", references: [id])
  groups    Group[]
}

model Group {
  id           Int                @default(autoincrement()) @id
  denomination String
  rating       Float?
  state        Boolean            @default(true)
  semester     Semester           @relation(references: [id])
  teacher      Teacher            @relation(references: [id])
  course       Course             @relation(references: [id])
  createdAt    DateTime           @default(now())
  updatedAt    DateTime           @updatedAt
  createdBy    User               @relation("groupCreatedByUser", references: [id])
  updatedBy    User               @relation("groupUpdatedByUser", references: [id])
  activities   Activity[]
  schedules    Schedule[]
  enrollments  EnrollmentDetail[]

  @@unique([semester, teacher, course, denomination])
}

model Enrollment {
  id              Int                @default(autoincrement()) @id
  weightedAverage Int?
  state           Boolean            @default(true)
  user            User               @relation(references: [id])
  semester        Semester           @relation(references: [id])
  createdAt       DateTime           @default(now())
  updatedAt       DateTime           @updatedAt
  groups          EnrollmentDetail[]

  @@unique([user, semester])
}

model EnrollmentDetail {
  id           Int        @default(autoincrement()) @id
  averageGrade Float?
  rating       Int?
  state        Boolean    @default(true)
  enrollment   Enrollment @relation(references: [id])
  group        Group      @relation(references: [id])
  grades       Grade[]

  @@unique([enrollment, group])
}

model TypeActivity {
  id         Int        @default(autoincrement()) @id
  name       String     @unique
  activities Activity[]
}

model Activity {
  id               Int          @default(autoincrement()) @id
  name             String
  weight           Float
  presentationDate DateTime?
  obligatory       Boolean      @default(true)
  state            Boolean      @default(true)
  parentActivity   Activity?    @relation("activity_has_activities", references: [id])
  typeActivity     TypeActivity @relation(references: [id])
  group            Group        @relation(references: [id])
  createdAt        DateTime     @default(now())
  updatedAt        DateTime     @updatedAt
  createdBy        User         @relation("activityCreatedByUser", references: [id])
  updatedBy        User         @relation("activityUpdatedByUser", references: [id])
  activities       Activity[]   @relation("activity_has_activities")
  grades           Grade[]
}

model Grade {
  id           Int              @default(autoincrement()) @id
  calification Float?
  confirmated  Boolean          @default(false)
  state        Boolean          @default(true)
  activity     Activity         @relation(references: [id])
  enrollment   EnrollmentDetail @relation(references: [id])
  createdAt    DateTime         @default(now())
  updatedAt    DateTime         @updatedAt
}

model Building {
  id         Int         @default(autoincrement()) @id
  name       String      @unique
  state      Boolean     @default(true)
  classrooms Classroom[]
}

model Classroom {
  id        Int        @default(autoincrement()) @id
  name      String?
  number    String?
  state     Boolean    @default(true)
  building  Building   @relation(references: [id])
  schedules Schedule[]
}

enum Day {
  LUNES
  MARTES
  MIERCOLES
  JUEVES
  VIERNES
  SABADO
}

model Schedule {
  id         Int       @default(autoincrement()) @id
  day        Day
  startHour  DateTime
  finishHour DateTime
  state      Boolean   @default(true)
  group      Group     @relation(references: [id])
  classroom  Classroom @relation(references: [id])
  createdAt  DateTime  @default(now())
  updatedAt  DateTime  @updatedAt
  createdBy  User      @relation("scheduleCreatedByUser", references: [id])
  updatedBy  User      @relation("scheduleUpdatedByUser", references: [id])
}

generator client {
  provider = "prisma-client-js"
}