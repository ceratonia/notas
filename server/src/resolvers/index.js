const Activity = require('./models/Activity');
const Building = require('./models/Building');
const Classroom = require('./models/Classroom');
const Course = require('./models/Course');
const Enrollment = require('./models/Enrollment');
const EnrollmentDetail = require('./models/EnrollmentDetail');
const Faculty = require('./models/Faculty');
const Grade = require('./models/Grade');
const Group = require('./models/Group');
const Schedule = require('./models/Schedule');
const School = require('./models/School');
const Semester = require('./models/Semester');
const Teacher = require('./models/Teacher');
const TypeActivity = require('./models/TypeActivity');
const User = require('./models/User');

module.exports = {
	Activity: Activity.getModel(),
	Building: Building.getModel(),
	Classroom: Classroom.getModel(),
	Course: Course.getModel(),
	Enrollment: Enrollment.getModel(),
	EnrollmentDetail: EnrollmentDetail.getModel(),
	Faculty: Faculty.getModel(),
	Grade: Grade.getModel(),
	Group: Group.getModel(),
	Schedule: Schedule.getModel(),
	School: School.getModel(),
	Semester: Semester.getModel(),
	Teacher: Teacher.getModel(),
	TypeActivity: TypeActivity.getModel(),
	User: User.getModel(),
	Query: {
		...Activity.getQueries(),
		...Building.getQueries(),
		...Classroom.getQueries(),
		...Course.getQueries(),
		...Enrollment.getQueries(),
		...EnrollmentDetail.getQueries(),
		...Faculty.getQueries(),
		...Grade.getQueries(),
		...Group.getQueries(),
		...Schedule.getQueries(),
		...School.getQueries(),
		...Semester.getQueries(),
		...Teacher.getQueries(),
		...TypeActivity.getQueries(),
		...User.getQueries()
	},
	Mutation: {
		...Activity.getMutations(),
		...Building.getMutations(),
		...Classroom.getMutations(),
		...Course.getMutations(),
		...Enrollment.getMutations(),
		...EnrollmentDetail.getMutations(),
		...Faculty.getMutations(),
		...Grade.getMutations(),
		...Group.getMutations(),
		...Schedule.getMutations(),
		...School.getMutations(),
		...Semester.getMutations(),
		...Teacher.getMutations(),
		...TypeActivity.getMutations(),
		...User.getMutations()
	}
};
