function getModel() {
	return {
		// Resolver for a faculty's schools
		schools: (parent, args, context) => {
			return context.prisma.faculty
				.findOne({ where: { id: parent.id } })
				.schools();
		}
	};
}

function getQueries() {
	return {
		// Returns all faculties
		faculties: (root, args, context, info) => {
			return context.prisma.faculty.findMany();
		}
	};
}
// 405.46 007032379607 09310209

function getMutations() {
	return {
		// Creates a faculty with a name.
		// Returns the created faculty.
		createFaculty: (parent, args, context, info) => {
			return context.prisma.faculty
				.create({
					data: { name: args.name }
				})
				.catch(err => null);
		},

		// Updates a faculty with an ID.
		// Can update name.
		// Returns the updated faculty with new data.
		updateFaculty: (parent, args, context, info) => {
			return context.prisma.faculty.update({
				where: { id: parseInt(args.id) },
				data: { name: args.name }
			});
		},

		// Deletes a faculty with an ID.
		// Returns the deleted faculty.
		deleteFaculty: (parent, args, context, info) => {
			return context.prisma.faculty
				.delete({
					where: { id: parseInt(args.id) }
				})
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
