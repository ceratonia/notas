const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { APP_SECRET, getUserId } = require('../../utils');

function getModel() {
	return {
		// Resolver for a user's semester in
		semesterIn: (parent, args, context) => {
			return context.prisma.user
				.findOne({ where: { id: parent.id } })
				.semesterIn();
		},
		// Resolver for a user's school
		school: (parent, args, context) => {
			return context.prisma.user
				.findOne({ where: { id: parent.id } })
				.school();
		},
		// Resolver for a user's enrollments
		enrollments: (parent, args, context) => {
			return context.prisma.user
				.findOne({ where: { id: parent.id } })
				.enrollments();
		}
	};
}

function getQueries() {
	return {
		users: (root, args, context, info) => {
			return context.prisma.user.findMany();
		},
		me: (root, args, context, info) => {
			const userId = getUserId(context);
			return context.prisma.user.findOne({
				where: { id: userId }
			});
		}
	};
}

function getMutations() {
	return {
		createUser: async (parent, args, context, info) => {
			const password = await bcrypt.hash(args.password, 10);
			const user = await context.prisma.user.create({
				data: {
					...args,
					password,
					semesterIn: { connect: { id: parseInt(args.semesterIn) } },
					school: { connect: { id: parseInt(args.school) } }
				}
			});
			const token = jwt.sign({ userId: user.id }, APP_SECRET);
			return { token, user };
		},
		updateUser: (parent, args, context, info) => {
			const data = {};
			if (args.nickname) data.nickname = args.nickname;
			if (args.password) data.password = args.password;
			if (args.email) data.email = args.email;
			if (args.name) data.name = args.name;
			if (args.lastname) data.lastname = args.lastname;
			if (args.gender) data.gender = args.gender;
			if (args.birthDate) data.birthDate = args.birthDate;
			if (args.photo) data.photo = args.photo;
			if (args.semesterIn) {
				data.semesterIn = {
					connect: { id: parseInt(args.semesterIn) }
				};
			}
			if (args.school) {
				data.school = { connect: { id: parseInt(args.school) } };
			}

			return context.prisma.user
				.update({ where: { id: parseInt(args.id) }, data })
				.catch(err => null);
		},
		deleteUser: (parent, args, context, info) => {
			return context.prisma.user
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		},
		login: async (root, args, context, info) => {
			const user = await context.prisma.user.findOne({
				where: { nickname: args.nickname }
			});

			if (!user) throw new Error('No such user found');

			const valid = await bcrypt.compare(args.password, user.password);

			if (!valid) throw new Error('Invalid password');

			return {
				token: jwt.sign({ userId: user.id }, APP_SECRET),
				user
			};
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
