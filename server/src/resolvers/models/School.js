function getModel() {
	return {
		// Resolver for a school's faculty
		faculty: (parent, args, context) => {
			return context.prisma.school
				.findOne({ where: { id: parent.id } })
				.faculty();
		},

		courses: (parent, args, context) => {
			return context.prisma.school
				.findOne({ where: { id: parent.id } })
				.courses();
		}
	};
}

function getQueries() {
	return {
		// Returns all schools
		schools: (root, args, context, info) => {
			return context.prisma.school.findMany();
		}
	};
}

function getMutations() {
	return {
		// Creates a school with a name and a associate faculty.
		// Returns the created school.
		createSchool: (parent, args, context, info) => {
			return context.prisma.school.create({
				data: {
					name: args.name,
					faculty: {
						connect: {
							id: parseInt(args.faculty)
						}
					}
				}
			});
		},
		// Updates a school with an ID.
		// Can update name and associate faculty.
		// Returns the updated school with new data.
		updateSchool: (parent, args, context, info) => {
			const data = {};

			// If the name field wasn't given then it isn't taken into account
			if (args.name) data.name = args.name;
			// If the faculty ID field wasn't given then it isn't taken into account
			if (args.faculty) {
				data.faculty = { connect: { id: parseInt(args.faculty) } };
			}

			return context.prisma.school
				.update({ where: { id: parseInt(args.id) }, data })
				.catch(err => null);
		},
		// Deletes a school with an ID.
		// Returns the deleted school.
		deleteSchool: (parent, args, context, info) => {
			return context.prisma.school
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
