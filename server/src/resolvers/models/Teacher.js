const { getUserId } = require('../../utils');
function getModel() {
	return {
		// Resolver for a teacher's groups
		groups: (parent, args, context) => {
			return context.prisma.teacher
				.findOne({ where: { id: parent.id } })
				.groups();
		},

		createdBy: (parent, args, context) => {
			return context.prisma.teacher
				.findOne({ where: { id: parent.id } })
				.createdBy();
		},

		updatedBy: (parent, args, context) => {
			return context.prisma.teacher
				.findOne({ where: { id: parent.id } })
				.updatedBy();
		}
	};
}

function getQueries() {
	return {
		// Returns all teachers
		teachers: (root, args, context, info) => {
			return context.prisma.teacher.findMany();
		}
	};
}

function getMutations() {
	return {
		// Creates a teacher with a name, a lastname and an email.
		// Returns the created teacher.
		createTeacher: (parent, args, context, info) => {
			const userId = getUserId(context);
			return context.prisma.teacher.create({
				data: {
					name: args.name,
					lastname: args.lastname,
					email: args.email,
					createdBy: { connect: { id: userId } },
					updatedBy: { connect: { id: userId } }
				}
			});
		},
		// Updates a teacher with an ID.
		// Can update name, lastname and email.
		// Returns the updated teacher with new data.
		updateTeacher: (parent, args, context, info) => {
			const userId = getUserId(context);
			const data = { updatedBy: { connect: { id: userId } } };

			// If the name field wasn't given then it isn't taken into account
			if (args.name) data.name = args.name;

			// If the lastname field wasn't given then it isn't taken into account
			if (args.lastname) data.lastname = args.lastname;

			// If the email field wasn't given then it isn't taken into account
			if (args.email) data.email = args.email;

			return context.prisma.teacher
				.update({ where: { id: parseInt(args.id) }, data })
				.catch(err => null);
		},
		// Deletes a teacher with an ID.
		// Returns the deleted teacher.
		deleteTeacher: (parent, args, context, info) => {
			return context.prisma.teacher
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
