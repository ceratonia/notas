function getModel() {
	return {
		building: (parent, args, context) => {
			return context.prisma.classroom
				.findOne({ where: { id: parent.id } })
				.building();
		}
	};
}

function getQueries() {
	return {
		// Returns all classrooms
		classrooms: (root, args, context, info) => {
			return context.prisma.classroom.findMany();
		}
	};
}

function getMutations() {
	return {
		// Creates a classroom with a name or number and a associate building.
		// Returns the created classroom.
		createClassroom: (parent, args, context, info) => {
			return context.prisma.classroom.create({
				data: {
					name: args.name,
					number: args.number,
					building: {
						connect: {
							id: parseInt(args.building)
						}
					}
				}
			});
		},

		// Updates a classroom with an ID.
		// Can update name, number and associate building.
		// Returns the updated classroom with new data.
		updateClassroom: (parent, args, context, info) => {
			const data = {};

			// If the name field wasn't given then it isn't taken into account
			if (args.name) data.name = args.name;
			// If the number field wasn't given then it isn't taken into account
			if (args.number) data.number = args.number;
			// If the building ID field wasn't given then it isn't taken into account
			if (args.building) {
				data.building = { connect: { id: parseInt(args.building) } };
			}

			return context.prisma.classroom
				.update({ where: { id: parseInt(args.id) }, data })
				.catch(err => null);
		},

		// Deletes a classroom with an ID.
		// Returns the deleted classroom.
		deleteClassroom: (parent, args, context, info) => {
			return context.prisma.classroom
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
