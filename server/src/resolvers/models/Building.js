function getModel() {
	return {};
}

function getQueries() {
	return {
		// Returns all buildings
		buildings: (root, args, context, info) => {
			return context.prisma.building.findMany();
		}
	};
}

function getMutations() {
	return {
		// Creates a building with a name.
		// Returns the created building.
		createBuilding: (parent, args, context, info) => {
			return context.prisma.building.create({
				data: { name: args.name }
			});
		},

		// Updates a building with an ID.
		// Can update name.
		// Returns the updated building with new data.
		updateBuilding: (parent, args, context, info) => {
			return context.prisma.building
				.update({
					where: { id: parseInt(args.id) },
					data: { name: args.name }
				})
				.catch(err => null);
		},

		// Deletes a building with an ID.
		// Returns the deleted building.
		deleteBuilding: (parent, args, context, info) => {
			return context.prisma.building
				.delete({
					where: { id: parseInt(args.id) }
				})
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
