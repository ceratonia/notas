const { getUserId } = require('../../utils');

function getModel() {
	return {
		user: (parent, args, context) => {
			return context.prisma.enrollment
				.findOne({ where: { id: parent.id } })
				.user();
		},
		semester: (parent, args, context) => {
			return context.prisma.enrollment
				.findOne({ where: { id: parent.id } })
				.semester();
		},
		groups: (parent, args, context) => {
			return context.prisma.enrollment
				.findOne({ where: { id: parent.id } })
				.groups();
		}
	};
}

function getQueries() {
	return {
		enrollments: (root, args, context, info) => {
			const userId = getUserId(context);
			return context.prisma.enrollment.findMany({
				where: { user: { id: userId } }
			});
		},
		enrollment: async (root, args, context, info) => {
			const userId = getUserId(context);
			return (
				await context.prisma.enrollment.findMany({
					where: {
						user: { id: userId },
						semester: { id: parseInt(args.semester) }
					}
				})
			)[0];
		}
	};
}

function getMutations() {
	return {
		createEnrollment: (parent, args, context, info) => {
			const userId = getUserId(context);
			return context.prisma.enrollment
				.create({
					data: {
						user: {
							connect: {
								id: userId
							}
						},
						semester: {
							connect: {
								id: parseInt(args.semester)
							}
						}
					}
				})
				.catch(err => null);
		},
		deleteEnrollment: (parent, args, context, info) => {
			return context.prisma.enrollment
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
