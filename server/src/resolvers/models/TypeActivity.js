function getModel() {
	return {
		// Resolver for a typeActivity's activities
		activities: (parent, args, context) => {
			return context.prisma.typeActivity
				.findOne({ where: { id: parent.id } })
				.activities();
		}
	};
}

function getQueries() {
	return {
		typesActivity: (root, args, context, info) => {
			return context.prisma.typeActivity.findMany();
		}
	};
}

function getMutations() {
	return {
		// Creates a type activity with a name.
		// Returns the created type activity.
		createTypeActivity: (parent, args, context, info) => {
			return context.prisma.typeActivity.create({
				data: { name: args.name }
			});
		},
		// Updates a type activity with an ID.
		// Can update name.
		// Returns the updated type activity with new data.
		updateTypeActivity: (parent, args, context, info) => {
			return context.prisma.typeActivity
				.update({
					where: { id: parseInt(args.id) },
					data: { name: args.name }
				})
				.catch(err => null);
		},
		// Deletes a type activity with an ID.
		// Returns the deleted type activity.
		deleteTypeActivity: (parent, args, context, info) => {
			return context.prisma.typeActivity
				.delete({
					where: { id: parseInt(args.id) }
				})
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
