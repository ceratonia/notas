const { getUserId } = require('../../utils');
function getModel() {
	return {
		parentActivity: (parent, args, context) => {
			return context.prisma.activity
				.findOne({ where: { id: parent.id } })
				.parentActivity();
		},
		activities: (parent, args, context) => {
			return context.prisma.activity
				.findOne({ where: { id: parent.id } })
				.activities();
		},
		typeActivity: (parent, args, context) => {
			return context.prisma.activity
				.findOne({ where: { id: parent.id } })
				.typeActivity();
		},
		group: (parent, args, context) => {
			return context.prisma.activity
				.findOne({ where: { id: parent.id } })
				.group();
		},
		createdBy: (parent, args, context) => {
			return context.prisma.activity
				.findOne({ where: { id: parent.id } })
				.createdBy();
		},
		updatedBy: (parent, args, context) => {
			return context.prisma.activity
				.findOne({ where: { id: parent.id } })
				.updatedBy();
		}
	};
}

function getQueries() {
	return {
		activities: (root, args, context, info) => {
			return context.prisma.activity.findMany();
		}
	};
}

function getMutations() {
	return {
		createActivity: (parent, args, context, info) => {
			const userId = getUserId(context);
			const data = {
				name: args.name,
				weight: args.weight,
				presentationDate: args.presentationDate,
				obligatory: args.obligatory,
				typeActivity: {
					connect: {
						id: parseInt(args.typeActivity)
					}
				},
				group: {
					connect: {
						id: parseInt(args.group)
					}
				},
				createdBy: {
					connect: {
						id: userId
					}
				},
				updatedBy: {
					connect: {
						id: userId
					}
				}
			};
			if (args.parentActivity)
				data.parentActivity = {
					connect: {
						id: parseInt(args.parentActivity)
					}
				};
			return context.prisma.activity.create({ data }).catch(err => null);
		},
		updateActivity: (parent, args, context, info) => {
			const userId = getUserId(context);
			const data = { updatedBy: { connect: { id: userId } } };
			if (args.name) data.name = args.name;
			if (args.weight) data.weight = args.weight;
			if (args.presentationDate)
				data.presentationDate = args.presentationDate;
			if (args.obligatory) data.obligatory = args.obligatory;
			if (args.parentActivity)
				data.parentActivity = {
					connect: { id: parseInt(args.parentActivity) }
				};
			if (args.typeActivity)
				data.typeActivity = {
					connect: { id: parseInt(args.typeActivity) }
				};
			return context.prisma.activity
				.update({
					where: { id: parseInt(args.id) },
					data
				})
				.catch(err => null);
		},
		deleteActivity: (parent, args, context, info) => {
			return context.prisma.activity
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
