function getModel() {
	return {
		enrollment: (parent, args, context) => {
			return context.prisma.enrollmentDetail
				.findOne({ where: { id: parent.id } })
				.enrollment();
		},
		group: (parent, args, context) => {
			return context.prisma.enrollmentDetail
				.findOne({ where: { id: parent.id } })
				.group();
		},
		grades: (parent, args, context) => {
			return context.prisma.enrollmentDetail
				.findOne({ where: { id: parent.id } })
				.grades();
		}
	};
}

function getQueries() {
	return {
		enrollmentDetails: (root, args, context, info) => {
			return context.prisma.enrollmentDetail.findMany();
		}
	};
}

function getMutations() {
	return {
		createEnrollmentDetail: (parent, args, context, info) => {
			return args.groups.map(
				async id =>
					await context.prisma.enrollmentDetail.create({
						data: {
							enrollment: {
								connect: { id: parseInt(args.enrollment) }
							},
							group: { connect: { id: parseInt(id) } }
						}
					})
			);
		},
		deleteEnrollmentDetail: (parent, args, context, info) => {
			return context.prisma.enrollmentDetail
				.delete({
					where: { id: parseInt(args.id) }
				})
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
