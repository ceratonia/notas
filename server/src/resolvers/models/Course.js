const { getUserId } = require('../../utils');

function getModel() {
	return {
		school: (parent, args, context) => {
			return context.prisma.course
				.findOne({ where: { id: parent.id } })
				.school();
		},
		prerequisiteOf: (parent, args, context) => {
			return context.prisma.course
				.findOne({ where: { id: parent.id } })
				.prerequisiteOf();
		},
		prerequisites: (parent, args, context) => {
			return context.prisma.course
				.findOne({ where: { id: parent.id } })
				.prerequisites();
		},
		createdBy: (parent, args, context) => {
			return context.prisma.course
				.findOne({ where: { id: parent.id } })
				.createdBy();
		},

		updatedBy: (parent, args, context) => {
			return context.prisma.course
				.findOne({ where: { id: parent.id } })
				.updatedBy();
		},
	};
}

function getQueries() {
	return {
		courses: (root, args, context, info) => {
			return context.prisma.course.findMany();
		},
	};
}

function getMutations() {
	return {
		createCourse: async (parent, args, context, info) => {
			const userId = getUserId(context);
			const data = {
				name: args.name,
				code: args.code,
				academicPhase: parseInt(args.academicPhase),
				credits: parseInt(args.credits),
				school: {
					connect: {
						id: parseInt(args.school),
					},
				},
				createdBy: { connect: { id: userId } },
				updatedBy: { connect: { id: userId } },
			};
			if (args.prerequisites) {
				data.prerequisites = {
					connect: args.prerequisites.map((id) => ({
						id: parseInt(id),
					})),
				};
			}
			const course = await context.prisma.course.create({
				data,
			});
			return course;
		},
		updateCourse: (parent, args, context, info) => {
			const userId = getUserId(context);
			const data = { updatedBy: { connect: { id: userId } } };
			if (args.name) data.name = args.name;
			if (args.code) data.code = args.code;
			if (args.academicPhase) data.academicPhase = args.academicPhase;
			if (args.credits) data.credits = args.credits;
			if (args.school)
				data.school = {
					connect: { id: parseInt(args.school) },
				};
			if (args.prerequisites)
				data.prerequisites = {
					connect: args.prerequisites.map((id) => ({
						id: parseInt(id),
					})),
				};
			return context.prisma.course
				.update({
					where: { id: parseInt(args.id) },
					data,
				})
				.catch((err) => null);
		},
		deleteCourse: (parent, args, context, info) => {
			return context.prisma.course
				.delete({ where: { id: parseInt(args.id) } })
				.then((err) => null);
		},
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations,
};
