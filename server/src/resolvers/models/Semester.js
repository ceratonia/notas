function getModel() {
	return {};
}

function getQueries() {
	return {
		// Returns all semesters
		semesters: (root, args, context, info) => {
			return context.prisma.semester.findMany();
		},
		actualSemester: async (root, args, context, info) => {
			const today = new Date();
			return (
				await context.prisma.semester.findMany({
					where: {
						startDate: { lte: today },
						finishDate: { gte: today }
					}
				})
			)[0];
		}
	};
}

function getMutations() {
	return {
		// Creates a semester with a name.
		// Returns the created semester.
		createSemester: (parent, args, context, info) => {
			return context.prisma.semester.create({
				data: {
					name: args.name,
					startDate: args.startDate,
					finishDate: args.finishDate
				}
			});
		},
		// Updates a semester with an ID.
		// Can update name, startDate and finishDate.
		// Returns the updated semester with new data.
		updateSemester: (parent, args, context, info) => {
			const data = {};

			if (args.name) data.name = args.name;
			if (args.startDate) data.startDate = args.startDate;
			if (args.finishDate) data.finishDate = args.finishDate;

			return context.prisma.semester
				.update({ where: { id: parseInt(args.id) }, data })
				.catch(err => null);
		},
		// Deletes a semester with an ID.
		// Returns the deleted semester.
		deleteSemester: (parent, args, context, info) => {
			return context.prisma.semester
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
