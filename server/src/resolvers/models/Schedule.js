const { getUserId } = require('../../utils');
function getModel() {
	return {
		group: (parent, args, context) => {
			return context.prisma.schedule
				.findOne({ where: { id: parent.id } })
				.group();
		},
		classroom: (parent, args, context) => {
			return context.prisma.schedule
				.findOne({ where: { id: parent.id } })
				.classroom();
		},
		createdBy: (parent, args, context) => {
			return context.prisma.schedule
				.findOne({ where: { id: parent.id } })
				.createdBy();
		},
		updatedBy: (parent, args, context) => {
			return context.prisma.schedule
				.findOne({ where: { id: parent.id } })
				.updatedBy();
		}
	};
}

function getQueries() {
	return {
		schedules: (root, args, context, info) => {
			return context.prisma.schedule.findMany({
				where: { group: { semester: { id: parseInt(args.semester) } } }
			});
		}
	};
}

function getMutations() {
	return {
		createSchedule: (parent, args, context, info) => {
			const userId = getUserId(context);
			return context.prisma.schedule
				.create({
					data: {
						day: args.day,
						startHour: args.startHour,
						finishHour: args.finishHour,
						group: {
							connect: {
								id: parseInt(args.group)
							}
						},
						classroom: {
							connect: {
								id: parseInt(args.classroom)
							}
						},
						createdBy: {
							connect: {
								id: userId
							}
						},
						updatedBy: {
							connect: {
								id: userId
							}
						}
					}
				})
				.catch(err => null);
		},
		updateSchedule: (parent, args, context, info) => {
			const userId = getUserId(context);
			const data = { updatedBy: { connect: { id: userId } } };
			if (args.day) data.day = args.day;
			if (args.startHour) data.startHour = args.startHour;
			if (args.finishHour) data.finishHour = args.finishHour;
			if (args.group)
				data.group = {
					connect: { id: parseInt(args.group) }
				};
			if (args.classroom)
				data.classroom = {
					connect: { id: parseInt(args.classroom) }
				};
			return context.prisma.schedule
				.update({ where: { id: parseInt(args.id) }, data })
				.catch(err => null);
		},
		deleteSchedule: (parent, args, context, info) => {
			return context.prisma.schedule
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
