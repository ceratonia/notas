const { getUserId } = require('../../utils');
function getModel() {
	return {
		semester: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.semester();
		},
		teacher: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.teacher();
		},
		course: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.course();
		},
		createdBy: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.createdBy();
		},
		updatedBy: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.updatedBy();
		},
		schedules: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.schedules();
		},
		activities: (parent, args, context) => {
			return context.prisma.group
				.findOne({ where: { id: parent.id } })
				.activities();
		}
	};
}

function getQueries() {
	return {
		groups: (root, args, context, info) => {
			return context.prisma.group.findMany({
				where: { semester: { id: parseInt(args.semester) } }
			});
		}
	};
}

function getMutations() {
	return {
		createGroup: (parent, args, context, info) => {
			const userId = getUserId(context);
			return context.prisma.group
				.create({
					data: {
						denomination: args.denomination,
						semester: { connect: { id: parseInt(args.semester) } },
						teacher: { connect: { id: parseInt(args.teacher) } },
						course: { connect: { id: parseInt(args.course) } },
						createdBy: { connect: { id: userId } },
						updatedBy: { connect: { id: userId } }
					}
				})
				.catch(err => null);
		},
		updateGroup: (parent, args, context, info) => {
			const userId = getUserId(context);
			let data = { updatedBy: { connect: { id: userId } } };
			if (args.denomination) data.denomination = args.denomination;
			if (args.semester)
				data.semester = { connect: { id: parseInt(args.semester) } };
			if (args.teacher)
				data.teacher = { connect: { id: parseInt(args.teacher) } };
			if (args.course)
				data.course = { connect: { id: parseInt(args.course) } };
			return context.prisma.group
				.update({ where: parseInt(args.id) }, data)
				.catch(err => null);
		},
		deleteGroup: (parent, args, context, info) => {
			return context.prisma.group
				.delete({ where: parseInt(args.id) })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
