function getModel() {
	return {
		activity: (parent, args, context) => {
			return context.prisma.grade
				.findOne({ where: { id: parent.id } })
				.activity();
		},
		enrollment: (parent, args, context) => {
			return context.prisma.grade
				.findOne({ where: { id: parent.id } })
				.enrollment();
		}
	};
}

function getQueries() {
	return {
		grades: (root, args, context, info) => {
			return context.prisma.grade.findMany();
		}
	};
}

function getMutations() {
	return {
		createGrade: (parent, args, context, info) => {
			return context.prisma.grade
				.create({
					data: {
						activity: { connect: { id: parseInt(args.activity) } },
						enrollment: {
							connect: { id: parseInt(args.enrollment) }
						}
					}
				})
				.catch(err => null);
		},
		deleteGrade: (parent, args, context, info) => {
			return context.prisma.grade
				.delete({ where: { id: parseInt(args.id) } })
				.catch(err => null);
		}
	};
}

module.exports = {
	getModel,
	getQueries,
	getMutations
};
