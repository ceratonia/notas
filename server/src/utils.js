const jwt = require('jsonwebtoken');
const APP_SECRET = 'idk-why-1-named-this-An0T4';

function getUserId(context) {
	const Authorization = context.request.get('Authorization');
	if (Authorization) {
		const token = Authorization.replace('Bearer ', '').replace(
			/['"]+/g,
			''
		);
		const { userId } = jwt.verify(token, APP_SECRET);
		return parseInt(userId);
	}

	throw new Error('Not authenticated');
}

module.exports = {
	APP_SECRET,
	getUserId
};
