const { GraphQLServer } = require('graphql-yoga'); // GraphQL Server
const { PrismaClient } = require('@prisma/client'); // Manipulate database data
const Resolvers = require('./resolvers');
// const Subscription = require('./resolvers/Subscription');
// Call to prisma client
const prisma = new PrismaClient();

// Implements the schema, resolvers and pass prisma client
const server = new GraphQLServer({
	typeDefs: './src/graphql/schema.graphql',
	resolvers: {
		...Resolvers
	},
	context: request => {
		return {
			...request,
			prisma
		};
	}
});

// Inititialize the server
server.start(() => console.log('Server is running on http://localhost:4000/'));
