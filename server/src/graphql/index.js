const fs = require('fs');
const path = require('path');

(function mergeSchema(path, nameFile = 'schema') {
	nameFile = nameFile + '.graphql';
	let pathFile = path + nameFile;
	const files = fs
		.readdirSync(path)
		.filter(f => f.endsWith('.graphql') && !f.endsWith(nameFile))
		.map(f => path + f);

	fs.writeFileSync(pathFile, '');

	let queries = '';
	let mutations = '';
	files.forEach(f => {
		let data = fs.readFileSync(f, 'utf8').split(/[{}]/);
		const queryIndex = data.findIndex(d => d.includes('type Query'));
		if (queryIndex > 0) {
			queries += data[queryIndex + 1];
			data.splice(queryIndex, 2);
		}
		const mutationIndex = data.findIndex(d => d.includes('type Mutation'));
		if (mutationIndex > 0) {
			mutations += data[mutationIndex + 1];
			data.splice(mutationIndex, 2);
		}
		const model = data[0] + '{' + data[1] + '}\n\n';
		fs.appendFileSync(pathFile, model, 'utf8');
	});
	queries = 'type Query {' + queries + '}\n\n';
	mutations = 'type Mutation {' + mutations + '}\n';
	fs.appendFileSync(pathFile, queries + mutations, 'utf8');
})(path.join(process.argv[1], '/'));
